package test.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneEditor {

  private String regexp;
  private String rexexpreplace;

  public PhoneEditor(String regexp, String rexexpreplace) {

    this.regexp = regexp;
    this.rexexpreplace = rexexpreplace;

  }


  public String phonesInString(String source) {

    Pattern p = Pattern.compile(this.regexp);
    Matcher m = p.matcher(source);
    String phones = "";
    while (m.find()) {
      phones = phones + m.group() + "\n";
    }
    return phoneInFormat(phones);

  }

  public String phoneInFormat(String inputString) {
    String outputString = null;
    outputString = inputString.replaceAll(this.rexexpreplace, "");
    return outputString;
  }

}

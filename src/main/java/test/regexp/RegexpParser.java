package test.regexp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class RegexpParser {

  final String REGEXP = "\\+\\d\\(\\d{3}\\) \\d{3} \\d{2} \\d{2}";
  final String REGEXPREPLACE = "[\\+\\(\\) ]";


  public void parse(String source) {
    
    try (FileReader reader = new FileReader(source);
        FileWriter writer = new FileWriter("Result.txt")) {
      BufferedReader buffer = new BufferedReader(reader);
      BufferedWriter bufferWriter = new BufferedWriter(writer);

      PhoneEditor phone = new PhoneEditor(REGEXP,REGEXPREPLACE);
      
      while (buffer.ready()) {
        String oneString = buffer.readLine();
        bufferWriter.write(phone.phonesInString(oneString));
      }
      
      buffer.close();
      bufferWriter.close();
      
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
